const botaoEnviar = document.querySelector(".botao-enviar")
const url = 'https://treinamento-ajax-api.herokuapp.com/messages/'
// Pega o histórico de mensagens
const msgHist = document.querySelector(".mensagens")
const messageInput = document.querySelector('.texto')
const autor = "Pedro Xavier"

botaoEnviar.addEventListener("click", (event) => {
    

    const body = {
        message: {
            content: messageInput.value,
            author: autor
        }
    }

    const config = {
        method: 'POST',
        body: JSON.stringify(body),
        headers: {"Content-Type": "application/json"}
    }

    fetch(url, config)
    .then(response => response.json())
    .then(message => {
        createPostDiv(message)
    })
    .catch((err) => {
        alert("Aconteceu um erro")
    })

})

function createPostDiv (message){
    const li = document.createElement("li")
    const txtArea = document.querySelector(".texto")

    li.className = "mensagem"
    li.innerHTML = ` 
        <h1>${message.author}</h1>
        <p class="texto-msg">${message.content}</p>
        <div class="botoes">
            <input type="button" value="Editar" class="Editar botao-msg" onclick="editar(this)")>
            <input type="button" value="Deletar" class="Deletar botao-msg" onclick="deletar(this, ${message.id})")>
        </div>
        <div class="botoes-modo-editar" hidden>
            <input>
            <input type="button" value="Enviar" class="Enviar botao-msg" onclick="editarTexto(this, ${message.id})">
        </div>`
    txtArea.value = ""

    msgHist.appendChild(li)
}

function deletar(element, id) {
    const config = {
        method: "DELETE"
    }

    fetch(url + id, config)
    .then(() => {
        element.parentNode.parentNode.remove()    
    })
    .catch((err) => {
        alert("Aconteceu um erro")
    })

}

function editar(elemento, id) {
    
    const parent = elemento.parentNode.parentNode

    // Esconder os botões normais
    parent.querySelector(".botoes").toggleAttribute("hidden")

    // Mostrar os botões de edição
    parent.querySelector(".botoes-modo-editar").toggleAttribute("hidden")
}

function editarTexto(elemento, id) {
    const parent = elemento.parentNode.parentNode
    const textArea = elemento.parentNode.children[0]

    const body = {
        message: {
            content: elemento.previousElementSibling.value,
            author: autor
        }
    }

    const config = {
        method: "PUT",
        body: JSON.stringify(body),
        headers: {"Content-Type": "application/json"}
    }

    fetch(url + id, config)
    .then(response => response.json())
    .then(message => {
        parent.querySelector("h1").innerText = `${message.author}`
        parent.querySelector(".texto-msg").innerText = message.content
    })
    .catch((err) => {
        alert("Aconteceu um erro")
    })

    // Editar o texto da mensagem
    

    // Esconder os elementos de editar novamente
    parent.querySelector(".botoes").toggleAttribute("hidden")

    // Tirar o atributo hidden dos botões Editar e Deletar
    parent.querySelector(".botoes-modo-editar").toggleAttribute("hidden")



    // Reseta o texto
    textArea.value = ""
}


const getMessages = () => {
    fetch(url)
    .then(response => response.json())
    .then(messages => {
        messages.forEach(message => {
            createPostDiv(message)
        });
    })
    .catch((err) => {
        alert("Aconteceu um erro")
    })
}

getMessages()